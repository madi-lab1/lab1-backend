import { IsNotEmpty, MinLength, Matches, IsInt } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @MinLength(5)
  name: string;

  //@IsInt({ min: 1 })
  @IsNotEmpty()
  price: number;
}
