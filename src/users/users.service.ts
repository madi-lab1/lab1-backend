import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Product } from './entities/user.entity';

let products: Product[] = [
  { id: 1, name: 'IPhone 13', price: 21900 },
  { id: 2, name: 'IPad Air 8', price: 32000 },
  { id: 3, name: 'MacBook Pro 5', price: 67000 },
];
let lastUserId = 4;
@Injectable()
export class UsersService {
  create(createUserDto: CreateUserDto) {
    const newProduct: Product = {
      id: lastUserId++,
      ...createUserDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    console.log('user ' + JSON.stringify(products[index]));
    console.log('update ' + JSON.stringify(updateUserDto));
    const updateUser: Product = {
      ...products[index],
      ...updateUserDto,
    };
    products[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteUser = products[index];
    products.splice(index, 1);
    return deleteUser;
  }
  reset() {
    products = [
      { id: 1, name: 'IPhone 13', price: 21900 },
      { id: 2, name: 'IPad Air 8', price: 32000 },
      { id: 3, name: 'MacBook Pro 5', price: 67000 },
    ];
    lastUserId = 4;
    return 'reset';
  }
}
